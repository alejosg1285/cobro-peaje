package com.alejosaag.cobropeaje;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.alejosaag.cobropeaje.POJO.Vehiculo;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView serviceType;
    private ListView vehiculeType;
    private EditText editCilindros;
    private RadioGroup radioPay;
    private RadioButton whenPay;
    private Button btn;
    private TextView basicValue;
    private TextView plusValue;
    private TextView ciliValue;
    private TextView otroValue;
    private TextView totalValue;
    private int servSelected;
    private int vehiSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editCilindros = findViewById(R.id.cntCilindros);
        radioPay = findViewById(R.id.radioPay);
        btn = findViewById(R.id.button);

        String[] services = new String[] { "Publico", "Particular", "Tren cañero" };
        String[] vehicules = new String[] { "Automovil", "Camioneta", "Buseta" };

        serviceType = findViewById(R.id.serviceType);
        ArrayAdapter<String> listServices = new ArrayAdapter<String>(MainActivity.this, R.layout.item_list, services);
        serviceType.setAdapter(listServices);
        serviceType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                servSelected = position;
                ListView lv = (ListView) parent;
                TextView tv = (TextView) lv.getChildAt(position);
                String s = tv.getText().toString();
                //Toast.makeText(MainActivity.this, "seleccionado: "+s+" en position "+position, Toast.LENGTH_LONG).show();
            }
        });

        vehiculeType = findViewById(R.id.vehiculeType);
        ArrayAdapter<String> listVehicules = new ArrayAdapter<String>(MainActivity.this, R.layout.item_list, vehicules);
        vehiculeType.setAdapter(listVehicules);
        vehiculeType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                vehiSelected = position;
                ListView lv = (ListView) parent;
                TextView tv = (TextView) lv.getChildAt(position);
                String s = tv.getText().toString();
                //Toast.makeText(MainActivity.this, "seleccionado: "+s+" en position "+position, Toast.LENGTH_LONG).show();
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularTarifaClick();
            }
        });
    }

    private void calcularTarifaClick() {
        String cilin = editCilindros.getText().toString();

        int radioSelected = radioPay.getCheckedRadioButtonId();
        whenPay = findViewById(radioSelected);
        String payBefore = whenPay.getText().toString();

        basicValue = findViewById(R.id.valueBasic);
        plusValue = findViewById(R.id.valuePlus);
        ciliValue = findViewById(R.id.valueCilin);
        otroValue = findViewById(R.id.valueOtro);
        totalValue = findViewById(R.id.valueTotal);

        Vehiculo vehiculo = new Vehiculo(String.valueOf(servSelected), String.valueOf(vehiSelected), Integer.parseInt(cilin), payBefore);
        vehiculo.calcularTarifaBase();
        vehiculo.calcularAdicional();
        vehiculo.calcularAdicionCilin();
        vehiculo.calcularDescPagoAnticipado();

        basicValue.setText(vehiculo.getTarifaBase());
        plusValue.setText(vehiculo.getTarifaAdicion());
        ciliValue.setText(vehiculo.getTarifaCilindraje());
        otroValue.setText(vehiculo.getTarifaDescuento());
        totalValue.setText(vehiculo.getTarifaTotal());

        //Toast.makeText(MainActivity.this, "vehiculo: "+vehiSelected+" servicio "+servSelected, Toast.LENGTH_LONG).show();
    }
}
