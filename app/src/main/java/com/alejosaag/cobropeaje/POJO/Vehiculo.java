package com.alejosaag.cobropeaje.POJO;

import java.text.DecimalFormat;

public class Vehiculo {
    private String tipoServicio;
    private String tipoVehiculo;
    private String pagoAntioipado;
    private int cilindraje;
    private int tarifaBase;
    private int tarifaAdicion;
    private int tarifaCilindraje;
    private double tarifaDescuento = 0;
    private double tarifaTotal;

    public Vehiculo(String tipoServicio, String tipoVehiculo, int cilindraje, String pagoAntioipado) {
        this.tipoServicio = tipoServicio;
        this.tipoVehiculo = tipoVehiculo;
        this.cilindraje = cilindraje;
        this.pagoAntioipado = pagoAntioipado;
    }

    public Vehiculo() {
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getCilindraje() {
        return String.format("%d cc", cilindraje);
    }

    public void setCilindraje(int cilindraje) {
        this.cilindraje = cilindraje;
    }

    public String getTarifaBase() {
        return String.format("$ %d", tarifaBase);
    }

    public String getTarifaAdicion() {
        return String.format("$ %d", tarifaAdicion);
    }

    public String getTarifaCilindraje() {
        return String.format("$ %d", tarifaCilindraje);
    }

    public String getTarifaDescuento() {
        DecimalFormat f = new DecimalFormat("##.00");
        return String.format("- $ %s", f.format(tarifaDescuento));
    }

    public String getTarifaTotal() {
        DecimalFormat f = new DecimalFormat("##.00");
        tarifaTotal = tarifaBase + tarifaAdicion + tarifaCilindraje - tarifaDescuento;
        return String.format("$ %s", f.format(tarifaTotal));
    }

    public void calcularTarifaBase() {
        switch (tipoServicio) {
            case "1": //particular
                tarifaBase = 17000;
                break;
            case "0": //publico
                tarifaBase = 22000;
                break;
            case "2": //cañero
                tarifaBase = 45000;
                break;
        }
    }

    public void calcularAdicional() {
        switch (tipoVehiculo) {
            case "0": //automovil
                tarifaAdicion = 3000;
                break;
            case "1": //camioneta
                tarifaAdicion = 5000;
                break;
            case "2": //buseta
                tarifaAdicion = 7000;
                break;
        }
    }

    public void calcularAdicionCilin() {
        if (cilindraje <= 1800)
            tarifaCilindraje = 0;
        else {
            int cilin = cilindraje - 1800;
            tarifaCilindraje = 250 * cilin;
        }
    }

    public void calcularDescPagoAnticipado() {
        if (pagoAntioipado.equals("Si")) {
            if (tipoServicio.equals("1")) {
                tarifaDescuento = tarifaBase * 0.05;
            } else if (tipoServicio.equals("0")) {
                tarifaDescuento = tarifaBase * 0.07;
            }
        }
    }
}
